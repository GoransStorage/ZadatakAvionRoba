package zadaci;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import model.Avion;

import java.io.IOException;
import java.util.List;


public class AvionNit extends  Thread {
    static Dao<Avion,Integer> avionDao;
    public static void main(String[] args) {

        ConnectionSource connectionSource = null;
        try {

            connectionSource = new JdbcConnectionSource(Konstante.DATABASE_URL);

            avionDao= DaoManager.createDao(connectionSource, Avion.class);

            List<Avion> avioni =avionDao.queryForAll();

            for(Avion a: avioni) {
                AvionNit n1 = new AvionNit(a);
                n1.start();
            }






        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (connectionSource != null) {
                try {
                    connectionSource.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }







        private Avion avion;
        public static boolean dozvoljenoPoletanje = true;
        public static Object av = new Object();



    public AvionNit(Avion avion) {
            this.avion = avion;
        }

        public Avion getAvion () {
            return avion;
        }

        public void setAvion (Avion avion){
            this.avion = avion;
        }

    public static boolean isDozvoljenoPoletanje() {
        return dozvoljenoPoletanje;
    }

    public static void setDozvoljenoPoletanje(boolean dozvoljenoPoletanje) {
        AvionNit.dozvoljenoPoletanje = dozvoljenoPoletanje;
    }

    @Override
        public void run () {
            System.out.println("pocinju provere za avion<" + avion.getId() + ">");

            try {
                Thread.sleep((int)(Math.random() *2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("avion<" + avion.getId() + "> je spreman za poletanje i trazi dozvolu za poletanje");


            boolean provera = false;
            do{
                synchronized (av) {
                    if (dozvoljenoPoletanje) {
                        dozvoljenoPoletanje = false;
                        provera = true;
                    }
                }
            }while (!provera);
        System.out.println("avion<" + avion.getId() + "> izlece na pistu i  polece");

        try {

            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("avion<" + avion.getId() + "> je poleteo");
        synchronized (av) {
            AvionNit.setDozvoljenoPoletanje(true);


            }
        }








}


